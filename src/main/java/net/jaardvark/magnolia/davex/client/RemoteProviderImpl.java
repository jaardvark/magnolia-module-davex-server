package net.jaardvark.magnolia.davex.client;

import java.io.InputStream;

import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.Workspace;

import org.apache.jackrabbit.rmi.repository.RmiRepositoryFactory;

import info.magnolia.repository.Provider;
import info.magnolia.repository.RepositoryNotInitializedException;
import info.magnolia.repository.definition.RepositoryDefinition;


/**
 * Implements a magnolia repository provider which connects to a remote repository via DAV or RMI.
 * 
 * Configure it something like this in your magnolia repositories.xml file:
 * 
 * <pre>
 *    %lt;RepositoryMapping>
 *    	... standard mappings ...
 *    	
 *    	%lt;!-- remote repository -->
 *      %lt;Map name="remote_website" repositoryName="remote" workspaceName="website" />
 *      %lt;Map name="remote_dms" repositoryName="remote" workspaceName="dms" />
 *    
 *    %lt;/RepositoryMapping>
 *
 *    %lt;!-- magnolia default repository -->
 *    %lt;Repository name="magnolia" provider="info.magnolia.jackrabbit.ProviderImpl" loadOnStartup="true">
 *        ... normal repo configuration ...
 *    %lt;/Repository>
 *
 *    %lt;Repository name="remote" provider="net.jaardvark.magnolia.davex.client.RemoteProviderImpl" loadOnStartup="true">
 *        %lt;param name="org.apache.jackrabbit.repository.uri" value="http://localhost:8080/magnoliaAuthor/.rmi/" />
 *        %lt;!--  
 *          %lt;param name="org.apache.jackrabbit.repository.uri" value="rmi://localhost:1199" />
 *        -->
 *        %lt;param name="user" value="admin" />
 *        %lt;param name="password" value="admin" />
 *        %lt;workspace name="website" />
 *        %lt;workspace name="dms" />
 *    %lt;/Repository>
 * </pre>
 * @author Richard Unger
 */
public class RemoteProviderImpl implements Provider {

	protected Repository repository;
	protected String systemUser = "admin";
	protected String systemPassword = "admin";

	@Override
	public void init(RepositoryDefinition repositoryDefinition) throws RepositoryNotInitializedException {
		if (repositoryDefinition.getParameters().containsKey("user"))
			systemUser = repositoryDefinition.getParameters().get("user");
		if (repositoryDefinition.getParameters().containsKey("password"))
			systemPassword = repositoryDefinition.getParameters().get("password");
		RmiRepositoryFactory factory = new RmiRepositoryFactory();
		try {
			repository = factory.getRepository(repositoryDefinition.getParameters());
		} catch (RepositoryException e) {
			throw new RepositoryNotInitializedException(e);
		}
	}

	@Override
	public Repository getUnderlyingRepository() throws RepositoryNotInitializedException {
		if (this.repository == null)
            throw new RepositoryNotInitializedException("Null repository");
		return repository;
	}

	@Override
	public void shutdownRepository() {
		// nothing special to do?
	}

	@Override
	public Session getSystemSession(String workspaceName) throws RepositoryException {
		if (this.repository == null)
            throw new RepositoryNotInitializedException("Null repository");
		return repository.login(new SimpleCredentials(systemUser, systemPassword.toCharArray()), workspaceName);
	}

	
	@Override
	public void registerNamespace(String prefix, String uri, Workspace workspace) throws RepositoryException {
		//throw new UnsupportedOperationException("At the moment, we don't support registering namespaces on remote repositories.");
	}

	@Override
	public void unregisterNamespace(String prefix, Workspace workspace) throws RepositoryException {
		//throw new UnsupportedOperationException("At the moment, we don't support registering namespaces on remote repositories.");
	}

	@Override
	public void registerNodeTypes() throws RepositoryException {
		//throw new UnsupportedOperationException("At the moment, we don't support registering node types on remote repositories.");
	}

	@Override
	public void registerNodeTypes(String configuration) throws RepositoryException {
		//throw new UnsupportedOperationException("At the moment, we don't support registering node types on remote repositories.");
	}

	@Override
	public void registerNodeTypes(InputStream stream) throws RepositoryException {
		//throw new UnsupportedOperationException("At the moment, we don't support registering node types on remote repositories.");
	}

	@Override
	public boolean registerWorkspace(String workspaceName) throws RepositoryException {
		Session s = getSystemSession(null);
		String[] workspaces = s.getWorkspace().getAccessibleWorkspaceNames();
		for (String workspace : workspaces)
			if (workspace.equals(workspaceName))
				return false;
		throw new UnsupportedOperationException("At the moment, we don't support registering new workspaces on remote repositories.");			
	}

}
