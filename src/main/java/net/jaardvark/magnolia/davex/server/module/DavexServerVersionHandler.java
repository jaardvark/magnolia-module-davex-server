package net.jaardvark.magnolia.davex.server.module;

import java.util.ArrayList;
import java.util.List;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.Task;

public class DavexServerVersionHandler extends DefaultModuleVersionHandler {
	
	@Override
	protected List<Task> getExtraInstallTasks(InstallContext installContext) {
		List<Task> extraTasks = new ArrayList<Task>();
		// nothing for now
		return extraTasks;
	}
	
}
