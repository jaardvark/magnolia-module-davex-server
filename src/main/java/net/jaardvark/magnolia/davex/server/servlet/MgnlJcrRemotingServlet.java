package net.jaardvark.magnolia.davex.server.servlet;

import info.magnolia.objectfactory.Components;
import info.magnolia.repository.RepositoryManager;

import javax.jcr.Repository;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.server.remoting.davex.JcrRemotingServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Exposes jackrabbit's remoting servlet for DAV2JCR access within magnolia.
 * Note that this is different from the standard webdav servlet, which provides a webdav interface
 * to certain magnolia content.
 * 
 * This servlet provides complete access to the repository via DAV remoting.
 * 
 * The servlet can only work with one physical repository, which defaults to the "magnolia" repository.
 * To use another repository use the initParameter "repositoryName" to set the repository to use.
 * 
 * @author Richard Unger
 */
public class MgnlJcrRemotingServlet extends JcrRemotingServlet {

	/**
	 * Logger
	 */	
	public static final Logger log = LoggerFactory.getLogger(MgnlJcrRemotingServlet.class);
	
	private static final long serialVersionUID = 1L;
	
	protected RepositoryManager repositoryManager;
	
	@Override
	protected Repository getRepository() {
		String repositoryId = StringUtils.defaultIfEmpty(getInitParameter("repositoryName"), "magnolia");
		return repositoryManager.getRepository(repositoryId);
	}

	@Override
	public void init() throws ServletException {
		super.init();
		repositoryManager = Components.getComponent(RepositoryManager.class);		
		// TODO we should install a sessionHandler and CredentialsProvider here which match with magnolia's session and user
		log.debug("MgnlJcrRemotingServlet inited!");
	}
	
}
