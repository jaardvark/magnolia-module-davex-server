package net.jaardvark.magnolia.davex.server.servlet;

import info.magnolia.objectfactory.Components;
import info.magnolia.repository.RepositoryManager;

import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.rmi.remote.RemoteRepository;
import org.apache.jackrabbit.rmi.server.ServerAdapterFactory;
import org.apache.jackrabbit.servlet.remote.RemoteBindingServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MgnlRMIRemotingServlet extends RemoteBindingServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * Logger
	 */
	public static final Logger log = LoggerFactory.getLogger(MgnlRMIRemotingServlet.class);
	
	protected RepositoryManager repositoryManager;

	protected RemoteRepository remote;
	
    protected RemoteRepository getRemoteRepository() throws ServletException {
        if (remote == null) {
            try {
            	ServerAdapterFactory factory = new ServerAdapterFactory();
            	String port = getInitParameter("rmiPort");
            	if (StringUtils.isNotBlank(port)){
            		log.info("Setting RMI port to "+port);
            		factory.setPortNumber(Integer.valueOf(port));            		
            	}
            	else
            		log.info("Setting RMI port to random port");
                String repositoryId = StringUtils.defaultIfEmpty(getInitParameter("repositoryName"), "magnolia");                
                remote = factory.getRemoteRepository(repositoryManager.getRepository(repositoryId));
            } catch (RemoteException e) {
            	log.error("Failed to create the remote repository reference",e);
                throw new ServletException("Failed to create the remote repository reference", e);
            }
        }
        return remote;
    }

	@Override
	public void init() throws ServletException {
		super.init();
		repositoryManager = Components.getComponent(RepositoryManager.class);		
		// TODO we should install a sessionHandler and CredentialsProvider here which match with magnolia's session and user
		log.debug("MgnlRMIRemotingServlet inited!");
	}
	
	@Override
	public void destroy() {
		super.destroy();
		if (remote!=null){
			log.debug("Shutting down RMI Repository");
            // Forcibly unexport the repository - TODO still hangs on shutdown more is needed...
            try {
                UnicastRemoteObject.unexportObject(remote, true);
            } catch (NoSuchObjectException e) {
                log.warn("Odd, the RMI repository was not exported", e);
            }
            // drop strong reference to remote repository
			remote = null;
		}
	}

}
